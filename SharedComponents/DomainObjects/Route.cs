﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArchitecturalBuildings.DomainObjects
{
    public class Route : DomainObject
    {
        public string Number { get; set; }
        public string Name { get; set; }
        public ArcBuildingOrganisation Organisation{ get; set; }
        public ArcBuildingType Type { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArchitecturalBuildings.DomainObjects
{
    public enum ArcBuildingType
    {
        Home_building,
        Administrative_building,
        Educational_institution,
        Trade_building,
        Administrative_home_buidling,
        Hotel,
        Multifunctional_complex,
        Cultural_business_center
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq.Expressions;


namespace ArchitecturalBuildings.DomainObjects.Ports
{
    public interface IReadOnlyArcBuildingsRepository
    {
        Task<ArcBuildingOrganisation> GetBuildingtOrganization(long id);
        Task<IEnumerable<ArcBuildingOrganisation>> GetAllBuildingOrganizations();
        Task<IEnumerable<ArcBuildingOrganisation>> QueryBuildingOrganizations(ICriteria<ArcBuildingOrganisation> criteria);

    }
    public interface IArcBuilginOrganizationRepository
    {
        Task AddBuildingOrganization(ArcBuildingOrganisation buildingOrganization);
        Task UpdateBuildingOrganization(ArcBuildingOrganisation buildingOrganization);
        Task RemoveBuildingOrganization(ArcBuildingOrganisation buildingOrganization);
    }

}
